package com.example.clinicadentaria;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.example.clinicadentaria.controller.*;
import com.example.clinicadentaria.helper.Utils;

public class MainActivity extends AppCompatActivity {

    Button btDentistas;
    Button btClientes;
    Button btEstatisticas;
    Button btConfiguracao;
    Button btConsultas;
    Button btClinicas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btDentistas = findViewById(R.id.btDentistas);
        btClientes = findViewById(R.id.btClientes);
        btEstatisticas = findViewById(R.id.btEstatisticas);
        btConfiguracao = findViewById(R.id.btConfiguracao);
        btConsultas = findViewById(R.id.btConsultas);
        btClinicas = findViewById(R.id.btClinicas);

        btClinicas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Clínicas",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, ClinicasMainActivity.class);
                startActivity(intent);
            }
        });

        btDentistas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Dentistas",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, EscolhaNifDentistasActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });

        btClientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Clientes",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, EscolhaNifClientesActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });

        btConsultas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Consultas",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, EscolhaNifServicosActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });

        btEstatisticas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Estatísticas",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, EstatisticasMainActivity.class);
                startActivity(intent);
            }
        });

        btConfiguracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Configuração IP Web Server",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_SETTINGS_ACTIVITY);
            }
        });

    }
}