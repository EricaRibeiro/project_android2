package com.example.clinicadentaria.exception;

public class CodigoConsultaInvalidoException extends RuntimeException{
    public CodigoConsultaInvalidoException(String s){super(s);}
}
