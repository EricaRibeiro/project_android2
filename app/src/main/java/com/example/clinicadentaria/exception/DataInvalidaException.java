package com.example.clinicadentaria.exception;

public class DataInvalidaException extends RuntimeException{
    public DataInvalidaException(String s){super(s);}
}
