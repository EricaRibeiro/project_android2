package com.example.clinicadentaria.exception;

public class NifInvalidoException extends RuntimeException{
    public NifInvalidoException(String s){super(s);}
}
