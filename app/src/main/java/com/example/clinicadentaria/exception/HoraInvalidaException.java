package com.example.clinicadentaria.exception;

public class HoraInvalidaException extends RuntimeException{
    public HoraInvalidaException(String s){super(s);}
}
