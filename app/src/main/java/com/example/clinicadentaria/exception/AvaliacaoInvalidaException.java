package com.example.clinicadentaria.exception;

public class AvaliacaoInvalidaException extends RuntimeException{
    public AvaliacaoInvalidaException(String s){super(s);}
}
