package com.example.clinicadentaria.exception;

public class TipoServicoInvalidoException extends RuntimeException{
    public TipoServicoInvalidoException(String s){super(s);}
}
