package com.example.clinicadentaria.exception;

public class IdentificacaoInvalidaException extends RuntimeException{
    public IdentificacaoInvalidaException(String s){super(s);}
}
