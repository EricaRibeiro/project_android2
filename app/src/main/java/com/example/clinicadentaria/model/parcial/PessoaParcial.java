package com.example.clinicadentaria.model.parcial;

public class PessoaParcial {

    private String nome;
    private long cartaoCidadao;

    public PessoaParcial(){}

    public PessoaParcial(String nome, long cartaoCidadao){
        this.nome = nome;
        this.cartaoCidadao = cartaoCidadao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCartaoCidadao() {
        return cartaoCidadao;
    }

    public void setCartaoCidadao(long cartaoCidadao) {
        this.cartaoCidadao = cartaoCidadao;
    }
}
