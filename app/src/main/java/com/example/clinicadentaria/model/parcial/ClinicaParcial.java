package com.example.clinicadentaria.model.parcial;

public class ClinicaParcial {

    private String nome;
    private long nif;

    public ClinicaParcial(){}

    public ClinicaParcial(String nome, long nif){
        this.nome = nome;
        this.nif = nif;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getNif() {
        return nif;
    }

    public void setNif(long nif) {
        this.nif = nif;
    }
}
