package com.example.clinicadentaria.model.parcial;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.model.Hora;

public class ServicoParcial {

    private String tipoServico;
    private Hora horaRealizacao;
    private Data dataRealizacao;
    private long identificacaoCliente;
    private int codigoConsulta;

    public ServicoParcial(){}

    public ServicoParcial(String tipoServico, Hora horaRealizacao, Data dataRealizacao, long identificacaoCliente, int codigoConsulta){
        this.tipoServico = tipoServico;
        this.horaRealizacao = horaRealizacao;
        this.dataRealizacao = dataRealizacao;
        this.identificacaoCliente = identificacaoCliente;
        this.codigoConsulta = codigoConsulta;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public Data getDataRealizacao() {
        return dataRealizacao;
    }

    public void setDataRealizacao(Data dataRealizacao) {
        this.dataRealizacao = dataRealizacao;
    }

    public long getIdentificacaoCliente() {
        return identificacaoCliente;
    }

    public void setIdentificacaoCliente(long identificacaoCliente) {
        this.identificacaoCliente = identificacaoCliente;
    }

    public Hora getHoraRealizacao() {
        return horaRealizacao;
    }

    public void setHoraRealizacao(Hora horaRealizacao) {
        this.horaRealizacao = horaRealizacao;
    }

    public int getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(int codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }
}
