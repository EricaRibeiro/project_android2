package com.example.clinicadentaria.model;
import com.example.clinicadentaria.exception.NifInvalidoException;

public class Clinica {
    private String nome;
    private Data dataConstituicao;
    private long nif;

    public Clinica(String nome, Data dataConstituicao, long nif){
        this.nome = nome;
        this.dataConstituicao = dataConstituicao;
        this.nif = nif;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Data getDataConstituicao() {
        return dataConstituicao;
    }

    public void setDataConstituicao(Data dataConstituicao) {
        this.dataConstituicao = dataConstituicao;
    }

    public long getNif() {
        return nif;
    }

    public void setNif(long nif) {
        if(nif >= 100000000 && nif <= 999999999)
            this.nif = nif;
        else
            throw new NifInvalidoException("O NIF da clínica deve ser composto por 9 dígitos");
    }
}
