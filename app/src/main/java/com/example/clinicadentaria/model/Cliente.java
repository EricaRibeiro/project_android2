package com.example.clinicadentaria.model;

import com.example.clinicadentaria.exception.IdentificacaoInvalidaException;

public class Cliente extends Pessoa{

    private long identificacaoDentista;

    public Cliente(){super();}

    public Cliente(String nome, long cartaoCidadao, Data dataNascimento, long identificacaoDentista) {

        super(nome, cartaoCidadao, dataNascimento);
        this.identificacaoDentista = identificacaoDentista;
    }

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        if(identificacaoDentista >= 100000000 && identificacaoDentista <= 999999999)
            this.identificacaoDentista = identificacaoDentista;
        else
            throw new IdentificacaoInvalidaException("A identificação do dentista (CC) deve ser composta por 9 dígitos");
    }
}
