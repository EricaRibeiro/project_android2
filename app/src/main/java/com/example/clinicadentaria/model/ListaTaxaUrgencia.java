package com.example.clinicadentaria.model;
import java.util.ArrayList;

public class ListaTaxaUrgencia {

    private ArrayList<TaxaUrgencia> taxaUrgencias;

    public ListaTaxaUrgencia(ArrayList<TaxaUrgencia> taxaUrgencias) {
        this.taxaUrgencias = taxaUrgencias;
    }

    public ListaTaxaUrgencia() {
    }

    public ArrayList<TaxaUrgencia> getTaxaUrgencias() {
        return taxaUrgencias;
    }

    public void setTaxaUrgencias(ArrayList<TaxaUrgencia> taxaUrgencias) {
        this.taxaUrgencias = taxaUrgencias;
    }
}
