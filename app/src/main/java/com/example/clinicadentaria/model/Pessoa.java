package com.example.clinicadentaria.model;

import com.example.clinicadentaria.exception.IdentificacaoInvalidaException;

public class Pessoa {

    private String nome;
    private long cartaoCidadao;
    private Data dataNascimento;

    public Pessoa(){}

    public Pessoa(String nome, long cartaoCidadao, Data dataNascimento){
        this.nome = nome;
        this.cartaoCidadao = cartaoCidadao;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCartaoCidadao() {
        return cartaoCidadao;
    }

    public void setCartaoCidadao(long cartaoCidadao) {
        if(cartaoCidadao >= 100000000 && cartaoCidadao <= 999999999)
            this.cartaoCidadao = cartaoCidadao;
        else
            throw new IdentificacaoInvalidaException("O cartão de cidadão deve ser composto por 9 dígitos");
    }

    public Data getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Data dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
