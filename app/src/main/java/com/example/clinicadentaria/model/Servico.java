package com.example.clinicadentaria.model;

import com.example.clinicadentaria.exception.CodigoConsultaInvalidoException;
import com.example.clinicadentaria.exception.HoraInvalidaException;
import com.example.clinicadentaria.exception.TipoServicoInvalidoException;

import java.util.Locale;

public class Servico {

    private String tipoServico;
    private int preco;
    private Data dataRealizacao;
    private Hora horaRealizacao;
    private long identificacaoCliente;
    private int codigoConsulta;

    public Servico(String tipoServico, int preco, Data dataRealizacao, Hora horaRealizacao, long identificacaoCliente, int codigoConsulta){
        this.tipoServico = tipoServico;
        this.preco = preco;
        this.dataRealizacao = dataRealizacao;
        if(Hora.validarMinutos(horaRealizacao.getMinutos()) && Hora.validarHora(horaRealizacao.getHora(), tipoServico, dataRealizacao))
            this.horaRealizacao = horaRealizacao;
        else{
            throw new HoraInvalidaException("Impossível proceder ao agendamento da consulta! A hora inserida não é válida!");
        }
        this.identificacaoCliente = identificacaoCliente;
        this.codigoConsulta = codigoConsulta;
    }
    public Servico(){}

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        String novaString = tipoServico.toLowerCase(Locale.ROOT).trim().replace(" ", "");
        if(!novaString.equals("limpeza") && !novaString.equals("consultaderotina") && !novaString.equals("colocaçãodeaparelho") && !novaString.equals("manutençãodeaparelho") && !novaString.equals("desvitalização") && !novaString.equals("urgência") && !novaString.equals("removerdente"))
            throw new TipoServicoInvalidoException("A clínica não oferece o serviço introduzido");
        else
            this.tipoServico = tipoServico;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public Data getDataRealizacao() {
        return dataRealizacao;
    }

    public void setDataRealizacao(Data dataRealizacao) {
        this.dataRealizacao = dataRealizacao;
    }

    public Hora getHoraRealizacao() {
        return horaRealizacao;
    }

    public void setHoraRealizacao(Hora horaRealizacao) {

        this.horaRealizacao = horaRealizacao;
    }

    public long getIdentificacaoCliente() {
        return identificacaoCliente;
    }

    public void setIdentificacaoCliente(long identificacaoCliente) {
        this.identificacaoCliente = identificacaoCliente;
    }

    public int getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(int codigoConsulta) {
        if(codigoConsulta >= 1000 && codigoConsulta <= 9999)
            this.codigoConsulta = codigoConsulta;
        else
            throw new CodigoConsultaInvalidoException("O código de consulta deve ser composto por 4 dígitos");
    }
}
