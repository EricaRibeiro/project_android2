package com.example.clinicadentaria.model.parcial;

public class ClienteParcial extends PessoaParcial{

    private long identificacaoDentista;

    public ClienteParcial(){super();}

    public ClienteParcial(String nome, long cc, long identificacaoDentista){
        super(nome, cc);
        this.identificacaoDentista = identificacaoDentista;
    }

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        this.identificacaoDentista = identificacaoDentista;
    }
}
