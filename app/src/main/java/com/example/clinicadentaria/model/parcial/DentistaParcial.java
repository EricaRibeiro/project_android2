package com.example.clinicadentaria.model.parcial;

public class DentistaParcial extends PessoaParcial{

    private long numeroCedula;

    public DentistaParcial(){
        super();
    }

    public DentistaParcial(String nome, long cartaoCidadao, long numeroCedula){
        super(nome, cartaoCidadao);
        this.numeroCedula = numeroCedula;
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        this.numeroCedula = numeroCedula;
    }
}
