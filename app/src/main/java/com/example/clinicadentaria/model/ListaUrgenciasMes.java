package com.example.clinicadentaria.model;
import java.util.ArrayList;

public class ListaUrgenciasMes {

    private ArrayList<UrgenciasMes> urgenciasMes;

    public ListaUrgenciasMes(ArrayList<UrgenciasMes> urgenciasMes) {
        this.urgenciasMes = urgenciasMes;
    }

    public ListaUrgenciasMes() {
    }

    public ArrayList<UrgenciasMes> getUrgenciasMes() {
        return urgenciasMes;
    }

    public void setUrgenciasMes(ArrayList<UrgenciasMes> urgenciasMes) {
        this.urgenciasMes = urgenciasMes;
    }

}
