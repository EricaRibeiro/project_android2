package com.example.clinicadentaria.model.parcial;
import java.util.ArrayList;

public class ListaDentistaParcial {

    public ListaDentistaParcial(ArrayList<DentistaParcial> dentistaParciais) {
        this.dentistaParciais = dentistaParciais;
    }

    public ListaDentistaParcial() {
    }
    private ArrayList<DentistaParcial> dentistaParciais;

    public ArrayList<DentistaParcial> getDentistaParciais() {
        return dentistaParciais;
    }

    public void setDentistaParciais(ArrayList<DentistaParcial> dentistaParciais) {
        this.dentistaParciais = dentistaParciais;
    }
}
