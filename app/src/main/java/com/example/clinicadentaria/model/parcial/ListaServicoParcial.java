package com.example.clinicadentaria.model.parcial;
import java.util.ArrayList;

public class ListaServicoParcial {

    public ListaServicoParcial(ArrayList<ServicoParcial> servicoParciais) {
        this.servicoParciais = servicoParciais;
    }

    public ListaServicoParcial() {
    }
    private ArrayList<ServicoParcial> servicoParciais;

    public ArrayList<ServicoParcial> getServicoParciais() {
        return servicoParciais;
    }

    public void setServicoParciais(ArrayList<ServicoParcial> servicoParciais) {
        this.servicoParciais = servicoParciais;
    }
}
