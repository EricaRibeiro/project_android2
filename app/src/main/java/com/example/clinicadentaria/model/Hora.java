package com.example.clinicadentaria.model;
import java.util.Calendar;
import java.util.Locale;

public class Hora {

    private int hora;
    private int minutos;

    public Hora(int hora, int minutos){
        this.hora = hora;
        this.minutos = minutos;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public static boolean validarHora(int hora, String consulta, Data data){
        String a = consulta.toLowerCase(Locale.ROOT).trim().replace(" ", "");
        if(a.equals("urgência")){
            if(hora < 8 || hora > 22)
               return false;
            else
                return true;
        }
        else {
            if((hora > 8 && hora < 12 || hora > 13 && hora < 20) && !validarDiaDaSemana(data))
                return true;
            else
                return false;
        }
    }

    public static boolean validarMinutos(int minutos){
        if(minutos % 30 != 0 ){
            return false;
        }
        return true;
    }

    public static boolean validarDiaDaSemana(Data d){
        Calendar c = Calendar.getInstance();
        c.set(d.getAno(), d.getMes() -1, d.getDia());
        return c.get(Calendar.DAY_OF_WEEK) == 1;
    }

    @Override
    public String toString(){
        return hora+":"+minutos;
    }
}
