package com.example.clinicadentaria.model;

public class Avaliacao {

    private int avaliacao;
    private long identificacaoCliente;

    public Avaliacao(int avaliacao, long identificacaoCliente){
        this.avaliacao = avaliacao;
        this.identificacaoCliente = identificacaoCliente;
    }

    public int getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(int avaliacao) {
            this.avaliacao = avaliacao;
    }

    public long getIdentificacaoCliente() {
        return identificacaoCliente;
    }

    public void setIdentificacaoCliente(long identificacaoCliente) {
        this.identificacaoCliente = identificacaoCliente;
    }

}
