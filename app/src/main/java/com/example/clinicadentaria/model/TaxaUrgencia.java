package com.example.clinicadentaria.model;
import androidx.appcompat.app.AppCompatActivity;

public class TaxaUrgencia extends AppCompatActivity {

    private String nomeClinica;
    private String taxaUrgencia;

    public TaxaUrgencia(String nomeClinica, String taxaUrgencia){
        this.taxaUrgencia = taxaUrgencia;
        this.nomeClinica = nomeClinica;
    }

    public TaxaUrgencia(){}

    public String getNomeClinica() {
        return nomeClinica;
    }

    public void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    public String getTaxaUrgencia() {
        return taxaUrgencia;
    }

    public void setTaxaUrgencia(String taxaUrgencia) {
        this.taxaUrgencia = taxaUrgencia;
    }
}
