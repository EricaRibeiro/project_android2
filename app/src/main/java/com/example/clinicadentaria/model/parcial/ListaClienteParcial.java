package com.example.clinicadentaria.model.parcial;

import java.util.ArrayList;

public class ListaClienteParcial {

    public ListaClienteParcial(ArrayList<ClienteParcial> clientesParciais) {
        this.clientesParciais = clientesParciais;
    }

    public ListaClienteParcial() {
    }

    private ArrayList<ClienteParcial> clientesParciais;

    public ArrayList<ClienteParcial> getClientesParciais() {
        return clientesParciais;
    }

    public void setClientesParciais(ArrayList<ClienteParcial> clientesParciais) {
        this.clientesParciais = clientesParciais;
    }
}
