package com.example.clinicadentaria.model;

import com.example.clinicadentaria.exception.IdentificacaoInvalidaException;

public class Dentista extends Pessoa{

    private long numeroCedula;

    public Dentista(){
        super();
    }

    public Dentista(String nome, long cartaoCidadao, Data dataNascimento, long numeroCedula){
        super(nome, cartaoCidadao, dataNascimento);
        this.numeroCedula = numeroCedula;
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        if(numeroCedula >= 100000000 && numeroCedula <= 999999999)
            this.numeroCedula = numeroCedula;
        else
            throw new IdentificacaoInvalidaException("O número de cédula deve ser composto por 9 dígitos");
    }
}
