package com.example.clinicadentaria.model.parcial;
import java.util.ArrayList;

public class ListaClinicaParcial {

    private ArrayList<ClinicaParcial> clinicasParciais;

    public ArrayList<ClinicaParcial> getClinicasParciais() {
        return clinicasParciais;
    }

    public void setClinicasParciais(ArrayList<ClinicaParcial> clinicasParciais) {
        this.clinicasParciais = clinicasParciais;
    }

    public ListaClinicaParcial(ArrayList<ClinicaParcial> pessoaPartials) {
        this.clinicasParciais = pessoaPartials;
    }
}
