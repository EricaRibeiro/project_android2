package com.example.clinicadentaria.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.model.UrgenciasMes;
import java.util.ArrayList;

public class ListViewAdapterUrgenciasMes extends BaseAdapter {

    private Context context;
    private int layoutId;
    private final ArrayList<UrgenciasMes> items;

    public ListViewAdapterUrgenciasMes(Context context, int layoutId, ArrayList<UrgenciasMes> items) {
        this.context = context;
        this.layoutId = layoutId;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final UrgenciasMes row = this.items.get(position);
        View itemView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(layoutId, null);
        } else {
            itemView = convertView;
        }

        TextView numeroUrgencias = itemView.findViewById(R.id.textViewNUrgencias);
        numeroUrgencias.setText(row.getNumeroUrgencias()+"");
        TextView nome = itemView.findViewById(R.id.textViewNome);
        nome.setText(row.getNomeClinica());

        return itemView;
    }
}
