package com.example.clinicadentaria.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.model.parcial.ServicoParcial;
import java.util.ArrayList;

public class ListViewAdapterServico extends BaseAdapter {

    private Context context;
    private int layoutId;
    private final ArrayList<ServicoParcial> items;

    public ListViewAdapterServico(Context context, int layoutId, ArrayList<ServicoParcial> items) {
        this.context = context;
        this.layoutId = layoutId;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ServicoParcial row = this.items.get(position);
        View itemView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(layoutId, null);
        } else {
            itemView = convertView;
        }

        TextView tipoServico = itemView.findViewById(R.id.textViewServico);
        tipoServico.setText(row.getTipoServico());
        TextView data = itemView.findViewById(R.id.textViewData);
        data.setText(row.getDataRealizacao().toString());
        TextView hora = itemView.findViewById(R.id.textViewHora);
        hora.setText(row.getHoraRealizacao().toString());
        TextView identificacaoCliente = itemView.findViewById(R.id.textViewCliente);
        identificacaoCliente.setText(row.getIdentificacaoCliente()+"");
        TextView codigoConsulta = itemView.findViewById(R.id.textViewCodigo);
        codigoConsulta.setText(row.getCodigoConsulta()+"");

        return itemView;
    }
}
