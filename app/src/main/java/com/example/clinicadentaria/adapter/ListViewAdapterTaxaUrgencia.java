package com.example.clinicadentaria.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.model.TaxaUrgencia;

import java.util.ArrayList;

public class ListViewAdapterTaxaUrgencia extends BaseAdapter {

    private Context context;
    private int layoutId;
    private final ArrayList<TaxaUrgencia> items;

    public ListViewAdapterTaxaUrgencia(Context context, int layoutId, ArrayList<TaxaUrgencia> items) {
        this.context = context;
        this.layoutId = layoutId;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final TaxaUrgencia row = this.items.get(position);
        View itemView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(layoutId, null);
        } else {
            itemView = convertView;
        }

        TextView taxaUrgencia = itemView.findViewById(R.id.textViewTUrgencias);
        taxaUrgencia.setText(row.getTaxaUrgencia()+"%");
        TextView nomeClinica = itemView.findViewById(R.id.textViewNome);
        nomeClinica.setText(row.getNomeClinica());

        return itemView;
    }
}
