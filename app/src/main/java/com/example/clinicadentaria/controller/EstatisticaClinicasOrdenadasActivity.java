package com.example.clinicadentaria.controller;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterClinica;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ListaClinicaParcialDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.parcial.ClinicaParcial;
import com.example.clinicadentaria.model.parcial.ListaClinicaParcial;
import com.example.clinicadentaria.network.GenericGetAsyncTask;
import com.example.clinicadentaria.network.HttpStatusCode;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class EstatisticaClinicasOrdenadasActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<ClinicaParcial> clinicaParciais;
    ListViewAdapterClinica adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica_clinicas_ordenadas);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        clinicaParciais = new ArrayList<ClinicaParcial>();
        adapter = new ListViewAdapterClinica(this, R.layout.listview_clinicas_item, clinicaParciais);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(EstatisticaClinicasOrdenadasActivity.this,"Click: details",Toast.LENGTH_SHORT).show();
                ClinicaParcial clinicaParcial = (ClinicaParcial) adapter.getItem(i);
                Intent intent = new Intent(EstatisticaClinicasOrdenadasActivity.this, ClinicasAcitivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DETAILS);
                intent.putExtra(Utils.ID, clinicaParcial.getNif());
                startActivity(intent);
            }
        });
        getClinicasFromWS();
    }

    private void getClinicasFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ListaClinicaParcialDTO listaPessoaPartialDTO = XmlHandler.deSerializeXML2ListaClinicaParcialDTO(httpResponse);
                ListaClinicaParcial pessoas = Converter.listaClinicaParcialDTO2ListaClinicaParcial(listaPessoaPartialDTO);
                return new Response(HttpStatusCode.OK, pessoas);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                clinicaParciais.clear();
                if (object instanceof ListaClinicaParcial) {
                    clinicaParciais.addAll(((ListaClinicaParcial) object).getClinicasParciais());
                }
                adapter.notifyDataSetChanged();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/ordemDasClinicas");
    }
}
