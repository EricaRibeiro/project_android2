package com.example.clinicadentaria.controller;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterDentista;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ListaDentistaParcialDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.parcial.DentistaParcial;
import com.example.clinicadentaria.model.parcial.ListaDentistaParcial;
import com.example.clinicadentaria.network.GenericGetAsyncTask;
import com.example.clinicadentaria.network.HttpStatusCode;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class EstatisticaDentistasOrdenadosActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<DentistaParcial> dentistaParciais;
    ListViewAdapterDentista adapter;
    String nifClincica;
    long nif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica_dentistas_ordenados);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb =  findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        dentistaParciais = new ArrayList<DentistaParcial>();
        adapter = new ListViewAdapterDentista(this, R.layout.listview_dentistas_item, dentistaParciais);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(EstatisticaDentistasOrdenadosActivity.this,"Click: details",Toast.LENGTH_SHORT).show();
                DentistaParcial dentistaParcial = (DentistaParcial) adapter.getItem(i);
                Intent intent = new Intent(EstatisticaDentistasOrdenadosActivity.this, DentistasActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DETAILS);
                intent.putExtra(Utils.ID, dentistaParcial.getCartaoCidadao());
                startActivity(intent);
            }
        });
        nifClincica =  Utils.getNifClinica(this);
        nif = Long.parseLong(nifClincica);
        getDentistasFromWS();
    }

    private void getDentistasFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ListaDentistaParcialDTO listaDentistaParcialDTO = XmlHandler.deSerializeXML2ListaDentistaParcialDTO(httpResponse);
                ListaDentistaParcial pessoas = Converter.listaDentistaParcialDTO2ListaDentistaParcial(listaDentistaParcialDTO);
                return new Response(HttpStatusCode.OK, pessoas);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                dentistaParciais.clear();
                if (object instanceof ListaDentistaParcial) {
                    dentistaParciais.addAll(((ListaDentistaParcial) object).getDentistaParciais());
                }
                adapter.notifyDataSetChanged();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/ordemDosDentistas");
    }
}
