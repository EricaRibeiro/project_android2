package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.dto.AvaliacaoDTO;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.Avaliacao;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;

public class EstatisticaAvaliarClinicaActivity extends AppCompatActivity {

    private Avaliacao avaliacao;

    private EditText etCC, etAvaliacao;
    private Button btOp, btCancel;
    private ProgressBar pb;
    private String nifClinica;
    private long nif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica_avaliar_clinica);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        avaliacao = null;
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);
        etCC = findViewById(R.id.etCC);
        etAvaliacao = findViewById(R.id.etAvaliacao);

        nifClinica = Utils.getNifClinica(this);
        nif = Long.parseLong(nifClinica);

        btOp = findViewById(R.id.btOp);
        btOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long idCliente;
                int avaliado;
                String exceptionMessage = "";
                boolean exception = false;
                try {
                    idCliente = Long.parseLong(etCC.getText().toString());
                    avaliado = Integer.parseInt(etAvaliacao.getText().toString());
                    Avaliacao a = new Avaliacao(avaliado, idCliente);
                    postAvaliacao2WS(a);
                }catch (Exception e){
                    exceptionMessage = e.getMessage();
                    exception = true;
                }
                if(exception == false) {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else{
                    Toast.makeText(EstatisticaAvaliarClinicaActivity.this,exceptionMessage,Toast.LENGTH_SHORT).show();
                }

            }
        });
        btCancel = findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });

        configureUI();
    }

    private void configureUI(){
        etCC.setEnabled(true);
        etAvaliacao.setEnabled(true);
        btOp.setText("Concluir");
    }

    private void postAvaliacao2WS(Avaliacao avaliacao) {
        AvaliacaoDTO avaliacaoDTO = Converter.avaliacao2AvaliacaoDTO(avaliacao);
        final String body = XmlHandler.serializeAvaliacaoDTO2XML(avaliacaoDTO);
        GenericPostAsyncTask task = new GenericPostAsyncTask(pb, this, body) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(EstatisticaAvaliarClinicaActivity.this, Utils.OPERATION_ADD_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/avaliarClinica/"+nif);
    }
}
