package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterDentista;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ListaDentistaParcialDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.parcial.DentistaParcial;
import com.example.clinicadentaria.model.parcial.ListaDentistaParcial;
import com.example.clinicadentaria.network.GenericGetAsyncTask;
import com.example.clinicadentaria.network.HttpStatusCode;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class DentistasMainActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<DentistaParcial> dentistasParcial;
    ListViewAdapterDentista adapter;
    String nifClincica;
    long nif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dentistas_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        dentistasParcial = new ArrayList<DentistaParcial>();
        adapter = new ListViewAdapterDentista(this, R.layout.listview_dentistas_item, dentistasParcial);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(DentistasMainActivity.this,"Click: details", Toast.LENGTH_SHORT).show();
                DentistaParcial dentistasParciais = (DentistaParcial) adapter.getItem(i);
                Intent intent = new Intent(DentistasMainActivity.this, DentistasActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DETAILS);
                intent.putExtra(Utils.ID, dentistasParciais.getCartaoCidadao());
                startActivity(intent);
            }
        });
        nifClincica =  Utils.getNifClinica(this);
        nif = Long.parseLong(nifClincica);
        getDentistasFromWS();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.add:
                Toast.makeText(DentistasMainActivity.this,"Option menu: Add",Toast.LENGTH_SHORT).show();
                intent = new Intent(DentistasMainActivity.this, DentistasActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_ADDING);
                startActivityForResult(intent, Utils.REQUEST_CODE_ADD_ACTIVITY);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int pos = info.position;
        Intent intent;
        DentistaParcial dentistaParcial = (DentistaParcial) adapter.getItem(pos);
        if (dentistaParcial != null) {
            switch (item.getItemId()) {
                case R.id.delete:
                    Toast.makeText(DentistasMainActivity.this,"Context menu: Delete",Toast.LENGTH_SHORT).show();
                    intent = new Intent(DentistasMainActivity.this, DentistasActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DELETING);
                    intent.putExtra(Utils.ID, dentistaParcial.getCartaoCidadao());
                    startActivityForResult(intent, Utils.REQUEST_CODE_DELETE_ACTIVITY);
                    return true;
                case R.id.edit:
                    Toast.makeText(DentistasMainActivity.this,"Context menu: Edit",Toast.LENGTH_SHORT).show();
                    intent = new Intent(DentistasMainActivity.this, DentistasActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_EDITING);
                    intent.putExtra(Utils.ID, dentistaParcial.getCartaoCidadao());
                    startActivityForResult(intent, Utils.REQUEST_CODE_EDIT_ACTIVITY);
                    return true;
                default:
                    break;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Utils.REQUEST_CODE_ADD_ACTIVITY:
            case Utils.REQUEST_CODE_DELETE_ACTIVITY:
            case Utils.REQUEST_CODE_EDIT_ACTIVITY:
                if (resultCode == Activity.RESULT_OK) {
                    getDentistasFromWS();
                }
                break;
            default:
        }
    }

    private void getDentistasFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ListaDentistaParcialDTO listaDentistaPartialDTO = XmlHandler.deSerializeXML2ListaDentistaParcialDTO(httpResponse);
                ListaDentistaParcial dentistas = Converter.listaDentistaParcialDTO2ListaDentistaParcial(listaDentistaPartialDTO);
                return new Response(HttpStatusCode.OK, dentistas);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                if (object instanceof ListaDentistaParcial) {
                    dentistasParcial.clear();
                    dentistasParcial.addAll(((ListaDentistaParcial) object).getDentistaParciais());
                }
                else{
                    dentistasParcial.clear();
                }
                adapter.notifyDataSetChanged();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/dentistas");
    }

}
