package com.example.clinicadentaria.controller;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterTaxaUrgencia;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ListaTaxaUrgenciaDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.ListaTaxaUrgencia;
import com.example.clinicadentaria.model.TaxaUrgencia;
import com.example.clinicadentaria.network.GenericGetAsyncTask;
import com.example.clinicadentaria.network.HttpStatusCode;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class EstatisticaTaxaUrgenciaActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<TaxaUrgencia> taxaUrgencias;
    ListViewAdapterTaxaUrgencia adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica_taxa_urgencia);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        taxaUrgencias = new ArrayList<TaxaUrgencia>();
        adapter = new ListViewAdapterTaxaUrgencia(this, R.layout.listview_taxa_urgencia_item, taxaUrgencias);
        lv.setAdapter(adapter);

        getClinicasFromWS();
    }

    private void getClinicasFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ListaTaxaUrgenciaDTO listaTaxaUrgenciaDTO = XmlHandler.deSerializeXML2ListaTaxaUrgenciaDTO(httpResponse);
                ListaTaxaUrgencia taxas = Converter.listaTaxaUrgenciaDTO2ListaTaxaUrgencia(listaTaxaUrgenciaDTO);
                return new Response(HttpStatusCode.OK, taxas);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                taxaUrgencias.clear();
                if (object instanceof ListaTaxaUrgencia) {
                    taxaUrgencias.addAll(((ListaTaxaUrgencia) object).getTaxaUrgencias());
                }
                adapter.notifyDataSetChanged();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/taxaUrgencia");
    }
}
