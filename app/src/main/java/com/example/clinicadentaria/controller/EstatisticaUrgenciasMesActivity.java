package com.example.clinicadentaria.controller;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterUrgenciasMes;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ListaUrgenciasMesDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.ListaUrgenciasMes;
import com.example.clinicadentaria.model.UrgenciasMes;
import com.example.clinicadentaria.network.GenericGetAsyncTask;
import com.example.clinicadentaria.network.HttpStatusCode;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class EstatisticaUrgenciasMesActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<UrgenciasMes> urgenciaMes;
    ListViewAdapterUrgenciasMes adapter;
    int mes;
    String mesEscolhido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica_urgencias_mes);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        urgenciaMes = new ArrayList<UrgenciasMes>();
        adapter = new ListViewAdapterUrgenciasMes(this, R.layout.listview_urgencias_mes_item, urgenciaMes);
        lv.setAdapter(adapter);

        mesEscolhido = Utils.getMesEscolhido(this);
        mes = Integer.parseInt(mesEscolhido);

        getNumeroUrgenciasFromWS();
    }

    private void getNumeroUrgenciasFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ListaUrgenciasMesDTO listaUrgenciasMesDTO = XmlHandler.deSerializeXML2ListaUrgenciasMesDTO(httpResponse);
                ListaUrgenciasMes urgencias = Converter.listaUrgenciasMesDTO2ListaUrgenciasMes(listaUrgenciasMesDTO);
                return new Response(HttpStatusCode.OK, urgencias);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                urgenciaMes.clear();
                if (object instanceof ListaUrgenciasMes) {
                    urgenciaMes.addAll(((ListaUrgenciasMes) object).getUrgenciasMes());
                }
                adapter.notifyDataSetChanged();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/numeroUrgenciasNumMes/"+mes);
    }
}
