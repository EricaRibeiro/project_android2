package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterServico;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ListaServicoParcialDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.parcial.ListaServicoParcial;
import com.example.clinicadentaria.model.parcial.ServicoParcial;
import com.example.clinicadentaria.network.GenericGetAsyncTask;
import com.example.clinicadentaria.network.HttpStatusCode;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class ServicosMainActivity extends AppCompatActivity {

    ListView lv;
    ProgressBar pb;
    ArrayList<ServicoParcial> servicosParcial;
    ListViewAdapterServico adapter;
    String nifClincica;
    long nif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicos_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        servicosParcial = new ArrayList<ServicoParcial>();
        adapter = new ListViewAdapterServico(this, R.layout.listview_servicos_item, servicosParcial);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ServicosMainActivity.this,"Click: details", Toast.LENGTH_SHORT).show();
                ServicoParcial servicosParciais = (ServicoParcial) adapter.getItem(i);
                Intent intent = new Intent(ServicosMainActivity.this, ServicosActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DETAILS);
                intent.putExtra(Utils.ID, servicosParciais.getCodigoConsulta());
                startActivity(intent);
            }
        });
        nifClincica =  Utils.getNifClinica(this);
        nif = Long.parseLong(nifClincica);
        getServicosFromWS();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.add:
                Toast.makeText(ServicosMainActivity.this,"Option menu: Add",Toast.LENGTH_SHORT).show();
                intent = new Intent(ServicosMainActivity.this, ServicosActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_ADDING);
                startActivityForResult(intent, Utils.REQUEST_CODE_ADD_ACTIVITY);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int pos = info.position;
        Intent intent;
        ServicoParcial servicoParcial = (ServicoParcial) adapter.getItem(pos);
        if (servicoParcial != null) {
            switch (item.getItemId()) {
                case R.id.delete:
                    Toast.makeText(ServicosMainActivity.this,"Context menu: Delete",Toast.LENGTH_SHORT).show();
                    intent = new Intent(ServicosMainActivity.this, ServicosActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DELETING);
                    intent.putExtra(Utils.ID, servicoParcial.getCodigoConsulta());
                    startActivityForResult(intent, Utils.REQUEST_CODE_DELETE_ACTIVITY);
                    return true;
                case R.id.edit:
                    Toast.makeText(ServicosMainActivity.this,"Context menu: Edit",Toast.LENGTH_SHORT).show();
                    intent = new Intent(ServicosMainActivity.this, ServicosActivity.class);
                    intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_EDITING);
                    intent.putExtra(Utils.ID, servicoParcial.getCodigoConsulta());
                    startActivityForResult(intent, Utils.REQUEST_CODE_EDIT_ACTIVITY);
                    return true;
                default:
                    break;
            }
        }
        return super.onContextItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Utils.REQUEST_CODE_ADD_ACTIVITY:
            case Utils.REQUEST_CODE_DELETE_ACTIVITY:
            case Utils.REQUEST_CODE_EDIT_ACTIVITY:
                if (resultCode == Activity.RESULT_OK) {
                    getServicosFromWS();
                }
                break;
            default:
        }
    }

    private void getServicosFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ListaServicoParcialDTO listaServicoParcialDTO = XmlHandler.deSerializeXML2ListaServicoParcialDTO(httpResponse);
                ListaServicoParcial servicos = Converter.listaServicoParcialDTO2ListaServicoParcial(listaServicoParcialDTO);
                return new Response(HttpStatusCode.OK, servicos);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                if (object instanceof ListaServicoParcial) {
                    servicosParcial.clear();
                    servicosParcial.addAll(((ListaServicoParcial) object).getServicoParciais());
                }
                else{
                    servicosParcial.clear();
                }
                adapter.notifyDataSetChanged();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/consultas");
    }
}
