package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ServicoDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.model.Hora;
import com.example.clinicadentaria.model.Servico;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.Calendar;

public class ServicosActivity extends AppCompatActivity {

    private int mode;
    private long id, nif;
    private Servico servico;

    private EditText etServico, etCodigo, etHoras, etMinutos, etCliente, etPreco;
    private DatePicker dp;
    private Button btOp, btCancel;
    private ProgressBar pb;
    private String nifClinica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicos);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        servico = null;
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);
        etServico = findViewById(R.id.etServico);
        etCodigo = findViewById(R.id.etCodigo);
        etCliente = findViewById(R.id.etCC);
        etHoras = findViewById(R.id.etHoras);
        etMinutos = findViewById(R.id.etMinutos);
        etPreco = findViewById(R.id.etPreco);

        dp = findViewById(R.id.dp);

        nifClinica = Utils.getNifClinica(this);
        nif = Long.parseLong(nifClinica);

        btOp = findViewById(R.id.btOp);
        btOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long idCliente;
                String tipoServico;
                Data data = null;
                Hora hora = null;
                int cod;
                int preco;
                String exceptionMessage = "";
                boolean exception = false;
                switch(mode){
                    case Utils.ACTIVITY_MODE_ADDING:
                        try {
                            idCliente = Long.parseLong(etCliente.getText().toString());
                            tipoServico = etServico.getText().toString();
                            data = new Data(dp.getDayOfMonth(), dp.getMonth(), dp.getYear());
                            hora = new Hora(Integer.parseInt(etHoras.getText().toString()), Integer.parseInt(etMinutos.getText().toString()));
                            cod = Integer.parseInt(etCodigo.getText().toString());
                            preco = Integer.parseInt(etPreco.getText().toString());
                            Servico a = new Servico(tipoServico, preco, data, hora, idCliente, cod);
                            postServico2WS(a);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    case Utils.ACTIVITY_MODE_DELETING:
                        deleteClinica2WS();
                        break;
                    case Utils.ACTIVITY_MODE_EDITING:
                        try {
                            idCliente = Long.parseLong(etCliente.getText().toString());
                            tipoServico = etServico.getText().toString();
                            data = new Data(dp.getDayOfMonth(), dp.getMonth(), dp.getYear());
                            hora = new Hora(Integer.parseInt(etHoras.getText().toString()), Integer.parseInt(etMinutos.getText().toString()));
                            cod = Integer.parseInt(etCodigo.getText().toString());
                            preco = Integer.parseInt(etPreco.getText().toString());
                            Servico e = new Servico(tipoServico, preco, data, hora, idCliente, cod);
                            putServico2WS(e);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    default:
                        Toast.makeText(ServicosActivity.this,Utils.UNKNOWN_MODE,Toast.LENGTH_SHORT).show();
                }
                if(exception == false) {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else{
                    Toast.makeText(ServicosActivity.this,exceptionMessage,Toast.LENGTH_SHORT).show();
                }

            }
        });
        btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });

        Intent intent = getIntent();
        mode  = intent.getIntExtra("MODE", Utils.ACTIVITY_MODE_NOTHING);
        configureUI();

        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING) {
            id = intent.getIntExtra("ID", Utils.ID_DEFAULT_VALUE);
            getServicoFromWS();
        }
    }

    private void configureUI(){
        switch(mode){
            case Utils.ACTIVITY_MODE_ADDING:
                etServico.setEnabled(true);
                etCodigo.setEnabled(true);
                etHoras.setEnabled(true);
                etMinutos.setEnabled(true);
                etCliente.setEnabled(true);
                etPreco.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Inserir");
                break;
            case Utils.ACTIVITY_MODE_DELETING:
                etServico.setEnabled(false);
                etCodigo.setEnabled(false);
                etHoras.setEnabled(false);
                etMinutos.setEnabled(false);
                etCliente.setEnabled(false);
                etPreco.setEnabled(false);
                dp.setEnabled(false);
                btOp.setText("Eliminar");
                break;
            case Utils.ACTIVITY_MODE_EDITING:
                etServico.setEnabled(true);
                etCodigo.setEnabled(true);
                etHoras.setEnabled(true);
                etMinutos.setEnabled(true);
                etCliente.setEnabled(true);
                etPreco.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Alterar");
                break;
            case Utils.ACTIVITY_MODE_DETAILS:
                etServico.setEnabled(false);
                etCodigo.setEnabled(false);
                etHoras.setEnabled(false);
                etMinutos.setEnabled(false);
                etCliente.setEnabled(false);
                etPreco.setEnabled(false);
                dp.setEnabled(false);
                btOp.setVisibility(View.GONE);
                btCancel.setVisibility(View.GONE);
                break;
            default:
                Toast.makeText(ServicosActivity.this,"Mode: desconhecido",Toast.LENGTH_SHORT).show();
        }
    }
    private void setDataUI(){
        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING){
            if(servico != null){
                etServico.setText(servico.getTipoServico());
                etCodigo.setText(servico.getCodigoConsulta()+"");
                Data data = servico.getDataRealizacao();
                dp.updateDate(data.getAno(), data.getMes(), data.getDia());
                etHoras.setText(servico.getHoraRealizacao().getHora()+"");
                etMinutos.setText(servico.getHoraRealizacao().getMinutos()+"");
                etCliente.setText(servico.getIdentificacaoCliente()+"");
                etPreco.setText(servico.getPreco()+"");
            }
            else{
                Toast.makeText(ServicosActivity.this,Utils.OPERATION_NO_DATA,Toast.LENGTH_SHORT).show();
                etServico.setText("");
                etCodigo.setText("");
                etHoras.setText("");
                etMinutos.setText("");
                etCliente.setText("");
                etPreco.setText("");
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                dp.updateDate(year, month, day);
            }
        }
    }

    private void getServicoFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ServicoDTO servicoDTO = XmlHandler.deSerializeXML2ServicoDTO(httpResponse);
                Servico servico = Converter.servicoDTO2Servico(servicoDTO);
                return new Response(HttpStatusCode.OK, servico);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                if (object instanceof Servico) {
                    servico = (Servico) object;
                    setDataUI();
                }
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/consulta/"+id);
    }

    private void postServico2WS(Servico servico) {
        ServicoDTO servicoDTO = Converter.servico2ServicoDTO(servico);
        final String body = XmlHandler.serializeServicoDTO2XML(servicoDTO);
        GenericPostAsyncTask task = new GenericPostAsyncTask(pb, this, body) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(ServicosActivity.this, Utils.OPERATION_ADD_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/consulta");
    }

    private void putServico2WS(Servico servico) {
        ServicoDTO servicoDTO = Converter.servico2ServicoDTO(servico);
        final String body = XmlHandler.serializeServicoDTO2XML(servicoDTO);
        GenericPutAsyncTask task = new GenericPutAsyncTask(pb, this, body) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(ServicosActivity.this, Utils.OPERATION_ADD_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/consultas/" + id + "/clinica/"+nif);
    }

    private void deleteClinica2WS() {
        GenericDeleteAsyncTask task = new GenericDeleteAsyncTask(pb, this) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(ServicosActivity.this, Utils.OPERATION_DELETE_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/" + nif+"/consultas/"+id);
    }
}
