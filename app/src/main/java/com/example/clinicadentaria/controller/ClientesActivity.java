package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.dto.ClienteDTO;
import com.example.clinicadentaria.dto.ClinicaDTO;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.Cliente;
import com.example.clinicadentaria.model.Clinica;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.Calendar;

public class ClientesActivity extends AppCompatActivity {

    private int mode;
    private long id, nif;
    private Cliente cliente;

    private EditText etCC, etNome, etIdentificacaoDentista;
    private DatePicker dp;
    private Button btOp, btCancel;
    private ProgressBar pb;
    private String nifClinica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        cliente = null;
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);
        etCC = findViewById(R.id.etCC);
        etNome = findViewById(R.id.etNome);
        etIdentificacaoDentista = findViewById(R.id.etIdentificacaoDentista);

        dp = findViewById(R.id.dp);

        nifClinica = Utils.getNifClinica(this);
        nif = Long.parseLong(nifClinica);

        btOp = findViewById(R.id.btOp);
        btOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long cc, idDentista;
                String nome;
                Data data = null;
                String exceptionMessage = "";
                boolean exception = false;
                switch(mode){
                    case Utils.ACTIVITY_MODE_ADDING:
                        try {
                            cc = Long.parseLong(etCC.getText().toString());
                            nome = etNome.getText().toString();
                            idDentista = Long.parseLong(etIdentificacaoDentista.getText().toString());
                            data = new Data(dp.getDayOfMonth(), dp.getMonth(), dp.getYear());
                            Cliente a = new Cliente(nome, cc, data, idDentista);
                            postCliente2WS(a);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    case Utils.ACTIVITY_MODE_DELETING:
                        deleteCliente2WS();
                        break;
                    case Utils.ACTIVITY_MODE_EDITING:
                        try {
                            cc = Long.parseLong(etCC.getText().toString());
                            nome = etNome.getText().toString();
                            idDentista = Long.parseLong(etIdentificacaoDentista.getText().toString());
                            data = new Data(dp.getDayOfMonth(), dp.getMonth(), dp.getYear());
                            Cliente e = new Cliente(nome, cc, data, idDentista);
                            putCliente2WS(e);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    default:
                        Toast.makeText(ClientesActivity.this,Utils.UNKNOWN_MODE,Toast.LENGTH_SHORT).show();
                }
                if(exception == false) {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else{
                    Toast.makeText(ClientesActivity.this,exceptionMessage,Toast.LENGTH_SHORT).show();
                }

            }
        });
        btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });

        Intent intent = getIntent();
        mode  = intent.getIntExtra("MODE", Utils.ACTIVITY_MODE_NOTHING);
        configureUI();

        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING) {
            id = intent.getLongExtra("ID", Utils.ID_DEFAULT_VALUE);
            getClienteFromWS();
        }
    }

    private void configureUI(){
        switch(mode){
            case Utils.ACTIVITY_MODE_ADDING:
                etCC .setEnabled(true);
                etIdentificacaoDentista.setEnabled(true);
                etNome.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Inserir");
                break;
            case Utils.ACTIVITY_MODE_DELETING:
                etCC .setEnabled(false);
                etIdentificacaoDentista.setEnabled(false);
                etNome.setEnabled(false);
                dp.setEnabled(false);
                btOp.setText("Eliminar");
                break;
            case Utils.ACTIVITY_MODE_EDITING:
                etCC .setEnabled(true);
                etIdentificacaoDentista.setEnabled(true);
                etNome.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Alterar");
                break;
            case Utils.ACTIVITY_MODE_DETAILS:
                etCC .setEnabled(false);
                etIdentificacaoDentista.setEnabled(false);
                etNome.setEnabled(false);
                dp.setEnabled(false);
                btOp.setVisibility(View.GONE);
                btCancel.setVisibility(View.GONE);
                break;
            default:
                Toast.makeText(ClientesActivity.this,"Mode: desconhecido",Toast.LENGTH_SHORT).show();
        }
    }
    private void setDataUI(){
        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING){
            if(cliente != null){
                etCC.setText(cliente.getCartaoCidadao()+"");
                etNome.setText(cliente.getNome());
                etIdentificacaoDentista.setText(cliente.getIdentificacaoDentista()+"");
                Data data = cliente.getDataNascimento();
                dp.updateDate(data.getAno(), data.getMes(), data.getDia());
            }
            else{
                Toast.makeText(ClientesActivity.this,Utils.OPERATION_NO_DATA,Toast.LENGTH_SHORT).show();
                etCC.setText("");
                etNome.setText("");
                etIdentificacaoDentista.setText("");
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                dp.updateDate(year, month, day);
            }
        }
    }

    private void getClienteFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ClienteDTO clienteDTO = XmlHandler.deSerializeXML2ClienteDTO(httpResponse);
                Cliente cliente = Converter.clienteDTO2Cliente(clienteDTO);
                return new Response(HttpStatusCode.OK, cliente);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                if (object instanceof Cliente) {
                    cliente = (Cliente) object;
                    setDataUI();
                }
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/pessoaCliente/"+id);
    }

    private void postCliente2WS(Cliente cliente) {
        ClienteDTO clienteDTO = Converter.cliente2ClienteDTO(cliente);
        final String body = XmlHandler.serializeClienteDTO2XML(clienteDTO);
        GenericPostAsyncTask task = new GenericPostAsyncTask(pb, this, body) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(ClientesActivity.this, Utils.OPERATION_ADD_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/cliente");
    }

    private void putCliente2WS(Cliente cliente) {
        ClienteDTO clienteDTO = Converter.cliente2ClienteDTO(cliente);
        final String body = XmlHandler.serializeClienteDTO2XML(clienteDTO);
        GenericPutAsyncTask task = new GenericPutAsyncTask(pb, this, body) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(ClientesActivity.this, Utils.OPERATION_ADD_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinicas/"+nif+"/clientes/"+id);
    }

    private void deleteCliente2WS() {
        GenericDeleteAsyncTask task = new GenericDeleteAsyncTask(pb, this) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(ClientesActivity.this, Utils.OPERATION_DELETE_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/pessoa/"+id);
    }
}
