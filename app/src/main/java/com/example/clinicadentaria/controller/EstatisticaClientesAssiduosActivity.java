package com.example.clinicadentaria.controller;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.adapter.ListViewAdapterCliente;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.ListaClienteParcialDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.parcial.ClienteParcial;
import com.example.clinicadentaria.model.parcial.ClinicaParcial;
import com.example.clinicadentaria.model.parcial.ListaClienteParcial;
import com.example.clinicadentaria.network.GenericGetAsyncTask;
import com.example.clinicadentaria.network.HttpStatusCode;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.ArrayList;

public class EstatisticaClientesAssiduosActivity extends AppCompatActivity {
    ListView lv;
    ProgressBar pb;
    ArrayList<ClienteParcial> clienteParciais;
    ListViewAdapterCliente adapter;
    String nifClinica;
    long nif;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatistica_clientes_assiduos);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);

        lv = findViewById(R.id.listView);
        clienteParciais = new ArrayList<ClienteParcial>();
        adapter = new ListViewAdapterCliente(this, R.layout.listview_clientes_item, clienteParciais);
        lv.setAdapter(adapter);
        registerForContextMenu(lv);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(EstatisticaClientesAssiduosActivity.this,"Click: details",Toast.LENGTH_SHORT).show();
                ClienteParcial clienteParcial = (ClienteParcial) adapter.getItem(i);
                Intent intent = new Intent(EstatisticaClientesAssiduosActivity.this, ClientesActivity.class);
                intent.putExtra(Utils.MODE,Utils.ACTIVITY_MODE_DETAILS);
                intent.putExtra(Utils.ID, clienteParcial.getCartaoCidadao());
                startActivity(intent);
            }
        });
        nifClinica = Utils.getNifClinica(this);
        nif = Long.parseLong(nifClinica);
        getClientesFromWS();
    }

    private void getClientesFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                ListaClienteParcialDTO listaClienteParcialDTO = XmlHandler.deSerializeXML2ListaClienteParcialDTO(httpResponse);
                ListaClienteParcial clientes = Converter.listaClienteParcialDTO2ListaClienteParcial(listaClienteParcialDTO);
                return new Response(HttpStatusCode.OK, clientes);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                clienteParciais.clear();
                if (object instanceof ListaClienteParcial) {
                    clienteParciais.addAll(((ListaClienteParcial) object).getClientesParciais());
                }
                adapter.notifyDataSetChanged();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/clientesAssiduos");
    }
}
