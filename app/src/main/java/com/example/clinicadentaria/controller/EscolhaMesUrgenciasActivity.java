package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.helper.Utils;

public class EscolhaMesUrgenciasActivity extends AppCompatActivity {

    Button btEscolhaMes, btCancel;
    EditText etMesEscolhido;
    String mes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolha_mes_urgencias);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btEscolhaMes = findViewById(R.id.btOp);

        mes = Utils.getMesEscolhido(this);
        etMesEscolhido = findViewById(R.id.etMesEscolhido);
        etMesEscolhido.setText(mes);

        btEscolhaMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mesDef = Integer.parseInt(etMesEscolhido.getText().toString());
                Utils.setMesEscolhido(EscolhaMesUrgenciasActivity.this, mesDef);
                Intent intent = new Intent();
                Intent intent1 = new Intent(EscolhaMesUrgenciasActivity.this, EstatisticaUrgenciasMesActivity.class);
                startActivity(intent1);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });
    }
}
