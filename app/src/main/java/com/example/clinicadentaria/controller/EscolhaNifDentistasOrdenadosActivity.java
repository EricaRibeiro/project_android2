package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.helper.Utils;

public class EscolhaNifDentistasOrdenadosActivity extends AppCompatActivity {

    Button btEscolhaNif, btCancel;
    String nif;
    EditText etNifServico;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escolha_nif_dentistas_ordenados);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btEscolhaNif = findViewById(R.id.btOp);
        etNifServico = findViewById(R.id.etNifEscolhido);

        nif =  Utils.getNifClinica(this);
        etNifServico = findViewById(R.id.etNifEscolhido);
        etNifServico.setText(nif);

        btEscolhaNif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long nifDef = Long.parseLong(etNifServico.getText().toString());
                Utils.setNifClinica(EscolhaNifDentistasOrdenadosActivity.this, nifDef);
                Intent intent = new Intent();
                Intent intent1 = new Intent(EscolhaNifDentistasOrdenadosActivity.this, EstatisticaDentistasOrdenadosActivity.class);
                startActivity(intent1);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });
    }
}
