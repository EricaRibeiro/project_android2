package com.example.clinicadentaria.controller;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.dto.Converter;
import com.example.clinicadentaria.dto.DentistaDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.model.Dentista;
import com.example.clinicadentaria.network.*;
import com.example.clinicadentaria.xml.XmlHandler;
import java.util.Calendar;

public class DentistasActivity extends AppCompatActivity {

    private int mode;
    private long id, nif;
    private Dentista dentista;

    private EditText etCC, etNome, etCedula;
    private DatePicker dp;
    private Button btOp, btCancel;
    private ProgressBar pb;
    private String nifClinica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dentistas);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        dentista = null;
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.INVISIBLE);
        etCC = findViewById(R.id.etCC);
        etNome = findViewById(R.id.etNome);
        etCedula = findViewById(R.id.etCedula);

        dp = findViewById(R.id.dp);

        nifClinica = Utils.getNifClinica(this);
        nif = Long.parseLong(nifClinica);

        btOp = findViewById(R.id.btOp);
        btOp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long cc, cedula;
                String nome;
                Data data = null;
                String exceptionMessage = "";
                boolean exception = false;
                switch(mode){
                    case Utils.ACTIVITY_MODE_ADDING:
                        try {
                            cc = Long.parseLong(etCC.getText().toString());
                            nome = etNome.getText().toString();
                            cedula = Long.parseLong(etCedula.getText().toString());
                            data = new Data(dp.getDayOfMonth(), dp.getMonth(), dp.getYear());
                            Dentista a = new Dentista(nome, cc, data, cedula);
                            postDentista2WS(a);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    case Utils.ACTIVITY_MODE_DELETING:
                        deleteDentista2WS();
                        break;
                    case Utils.ACTIVITY_MODE_EDITING:
                        try {
                            cc = Long.parseLong(etCC.getText().toString());
                            nome = etNome.getText().toString();
                            cedula = Long.parseLong(etCedula.getText().toString());
                            data = new Data(dp.getDayOfMonth(),dp.getMonth(),dp.getYear());
                            Dentista e = new Dentista(nome, cc, data, cedula);
                            putDentista2WS(e);
                        }catch (Exception e){
                            exceptionMessage = e.getMessage();
                            exception = true;
                        }
                        break;
                    default:
                        Toast.makeText(DentistasActivity.this,Utils.UNKNOWN_MODE,Toast.LENGTH_SHORT).show();
                }
                if(exception == false) {
                    Intent intent = new Intent();
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }else{
                    Toast.makeText(DentistasActivity.this,exceptionMessage,Toast.LENGTH_SHORT).show();
                }

            }
        });
        btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_CANCELED, intent);
                finish();
            }
        });

        Intent intent = getIntent();
        mode  = intent.getIntExtra("MODE", Utils.ACTIVITY_MODE_NOTHING);
        configureUI();

        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING) {
            id = intent.getLongExtra("ID", Utils.ID_DEFAULT_VALUE);
            getDentistaFromWS();
        }
    }

    private void configureUI(){
        switch(mode){
            case Utils.ACTIVITY_MODE_ADDING:
                etCC.setEnabled(true);
                etNome.setEnabled(true);
                etCedula.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Inserir");
                break;
            case Utils.ACTIVITY_MODE_DELETING:
                etCC.setEnabled(false);
                etNome.setEnabled(false);
                etCedula.setEnabled(false);
                dp.setEnabled(false);
                btOp.setText("Eliminar");
                break;
            case Utils.ACTIVITY_MODE_EDITING:
                etCC.setEnabled(true);
                etNome.setEnabled(true);
                etCedula.setEnabled(true);
                dp.setEnabled(true);
                btOp.setText("Alterar");
                break;
            case Utils.ACTIVITY_MODE_DETAILS:
                etCC.setEnabled(false);
                etNome.setEnabled(false);
                etCedula.setEnabled(false);
                dp.setEnabled(false);
                btOp.setVisibility(View.GONE);
                btCancel.setVisibility(View.GONE);
                break;
            default:
                Toast.makeText(DentistasActivity.this,"Mode: desconhecido",Toast.LENGTH_SHORT).show();
        }
    }
    private void setDataUI(){
        if(mode == Utils.ACTIVITY_MODE_DELETING || mode == Utils.ACTIVITY_MODE_DETAILS || mode == Utils.ACTIVITY_MODE_EDITING){
            if(dentista != null){
                etCC.setText(dentista.getCartaoCidadao()+"");
                etNome.setText(dentista.getNome());
                etCedula.setText(dentista.getNumeroCedula()+"");
                Data data = dentista.getDataNascimento();
                dp.updateDate(data.getAno(), data.getMes(), data.getDia());
            }
            else{
                Toast.makeText(DentistasActivity.this,Utils.OPERATION_NO_DATA,Toast.LENGTH_SHORT).show();
                etCC.setText("");
                etNome.setText("");
                etCedula.setText("");
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH);
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                dp.updateDate(year, month, day);
            }
        }
    }

    private void getDentistaFromWS() {
        GenericGetAsyncTask task = new GenericGetAsyncTask(pb, this) {
            protected Response getResponseObject(String httpResponse) {
                DentistaDTO dentistaDTO = XmlHandler.deSerializeXML2DentistaDTO(httpResponse);
                Dentista dentista = Converter.dentistaDTO2Dentista(dentistaDTO);
                return new Response(HttpStatusCode.OK, dentista);
            }

            protected void onPostExecuteProcessDataUI(Object object) {
                if (object instanceof Dentista) {
                    dentista = (Dentista) object;
                    setDataUI();
                }
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/" +nif+"/dentista/"+id);
    }
    private void postDentista2WS(Dentista dentista) {
        DentistaDTO dentistaDTO = Converter.dentista2DentistaDTO(dentista);
        final String body = XmlHandler.serializeDentistaDTO2XML(dentistaDTO);
        GenericPostAsyncTask task = new GenericPostAsyncTask(pb, this, body) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(DentistasActivity.this, Utils.OPERATION_ADD_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/"+nif+"/dentista");
    }
    private void putDentista2WS(Dentista dentista) {
        DentistaDTO dentistaDTO = Converter.dentista2DentistaDTO(dentista);
        final String body = XmlHandler.serializeDentistaDTO2XML(dentistaDTO);
        GenericPutAsyncTask task = new GenericPutAsyncTask(pb, this, body) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(DentistasActivity.this, Utils.OPERATION_ADD_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinicas/" + nif+"/dentistas/"+id);
    }
    private void deleteDentista2WS() {
        GenericDeleteAsyncTask task = new GenericDeleteAsyncTask(pb, this) {
            protected void onPostExecuteProcessDataUI(Object object) {
                Toast.makeText(DentistasActivity.this, Utils.OPERATION_DELETE_SUCESSS, Toast.LENGTH_LONG).show();
            }
        };
        String address = Utils.getWSAddress(this);
        task.execute(address + "/clinica/" + nif+"/pessoa/"+id);
    }
}
