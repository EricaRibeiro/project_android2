package com.example.clinicadentaria.controller;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import com.example.clinicadentaria.R;
import com.example.clinicadentaria.helper.Utils;

public class EstatisticasMainActivity extends AppCompatActivity {

    Button btOrdemDasClinicas, btOrdemDosDentistas, btClinicaMaisUrgencias, btClientesAssiduosNumaClinica, btRankingDentistas, btAvaliarClinica, btClinicasFavoritas, btTaxaUrgencia, btUrgenciasNumMes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estatisticas_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        btOrdemDasClinicas = findViewById(R.id.btOrdemDasClinicas);
        btOrdemDasClinicas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EstatisticaClinicasOrdenadasActivity.class);
                startActivity(intent);
            }
        });

        btOrdemDosDentistas = findViewById(R.id.btOrdemDosDentistas);
        btOrdemDosDentistas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EscolhaNifDentistasOrdenadosActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });

        btClinicaMaisUrgencias = findViewById(R.id.btClinicaMaisUrgencias);
        btClinicaMaisUrgencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EstatisticaClinicaMaisUrgenciasActivity.class);
                startActivity(intent);
            }
        });

        btClientesAssiduosNumaClinica = findViewById(R.id.btClientesAssiduosNumaClinica);
        btClientesAssiduosNumaClinica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EscolhaNifClientesAssiduosActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });

        btRankingDentistas = findViewById(R.id.btRankingDentistas);
        btRankingDentistas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EscolhaNifDentistasComMaisClientesActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });

        btAvaliarClinica = findViewById(R.id.btAvaliarClinica);
        btAvaliarClinica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EscolhaNifAvaliacaoActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });

        btClinicasFavoritas = findViewById(R.id.btClinicasFavoritas);
        btClinicasFavoritas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EstatisticaTopClinicasActivity.class);
                startActivity(intent);
            }
        });

        btTaxaUrgencia = findViewById(R.id.btTaxadeUrgencia);
        btTaxaUrgencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EstatisticaTaxaUrgenciaActivity.class);
                startActivity(intent);
            }
        });

        btUrgenciasNumMes = findViewById(R.id.btUrgenciasNumMes);
        btUrgenciasNumMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EstatisticasMainActivity.this, EscolhaMesUrgenciasActivity.class);
                startActivityForResult(intent, Utils.REQUEST_CODE_NIF_ACTIVITY);
            }
        });
    }
}
