package com.example.clinicadentaria.xml;
import com.example.clinicadentaria.dto.*;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.StringWriter;

public class XmlHandler {

    public static String serializeErroDTO2XML(ErroDTO erroDTO) {
        StringWriter writer = new StringWriter();
        if (erroDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(erroDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }
    public static ErroDTO deSerializeXML2ErroDTO(String xmlData) {
        ErroDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ErroDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static ListaClinicaParcialDTO deSerializeXML2ListaClinicaParcialDTO(String xmlData) {
        ListaClinicaParcialDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaClinicaParcialDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static ClinicaDTO deSerializeXML2ClinicaDTO(String xmlData) {
        ClinicaDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ClinicaDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String serializeClinicaDTO2XML(ClinicaDTO clinicaDTO) {
        StringWriter writer = new StringWriter();
        if (clinicaDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(clinicaDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

    public static ListaDentistaParcialDTO deSerializeXML2ListaDentistaParcialDTO(String xmlData) {
        ListaDentistaParcialDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaDentistaParcialDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static DentistaDTO deSerializeXML2DentistaDTO(String xmlData) {
        DentistaDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(DentistaDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String serializeDentistaDTO2XML(DentistaDTO dentistaDTO) {
        StringWriter writer = new StringWriter();
        if (dentistaDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(dentistaDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

    public static ClienteDTO deSerializeXML2ClienteDTO(String xmlData) {
        ClienteDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ClienteDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String serializeClienteDTO2XML(ClienteDTO clienteDTO) {
        StringWriter writer = new StringWriter();
        if (clienteDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(clienteDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

    public static ListaClienteParcialDTO deSerializeXML2ListaClienteParcialDTO(String xmlData) {
        ListaClienteParcialDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaClienteParcialDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static ListaServicoParcialDTO deSerializeXML2ListaServicoParcialDTO(String xmlData) {
        ListaServicoParcialDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaServicoParcialDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static ServicoDTO deSerializeXML2ServicoDTO(String xmlData) {
        ServicoDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ServicoDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static String serializeServicoDTO2XML(ServicoDTO servicoDTO) {
        StringWriter writer = new StringWriter();
        if (servicoDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(servicoDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

    public static String serializeAvaliacaoDTO2XML(AvaliacaoDTO avaliacaoDTO) {
        StringWriter writer = new StringWriter();
        if (avaliacaoDTO != null) {
            Serializer serializer = new Persister();
            try {
                serializer.write(avaliacaoDTO, writer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }

    public static ListaTaxaUrgenciaDTO deSerializeXML2ListaTaxaUrgenciaDTO(String xmlData) {
        ListaTaxaUrgenciaDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaTaxaUrgenciaDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

    public static ListaUrgenciasMesDTO deSerializeXML2ListaUrgenciasMesDTO(String xmlData) {
        ListaUrgenciasMesDTO data = null;
        if(xmlData != null){
            Serializer serializer = new Persister();
            try {
                data = serializer.read(ListaUrgenciasMesDTO.class, xmlData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return data;
    }

}
