package com.example.clinicadentaria.network;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.clinicadentaria.dto.ErroDTO;
import com.example.clinicadentaria.helper.Response;
import com.example.clinicadentaria.helper.Utils;
import com.example.clinicadentaria.xml.XmlHandler;

public class GenericGetAsyncTask extends AsyncTask<String, Void, Response> {

    private ProgressBar pb;
    private Context context;

    public GenericGetAsyncTask(ProgressBar pb, Context context) {
        this.pb = pb;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        pb.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    protected Response doInBackground(String... params) {
        Response response = null;
        HttpRequest httpRequest = new HttpRequest(HttpRequestType.GET, params[0], "");
        HttpResponse httpResponse = HttpConnection.makeRequest(httpRequest);
        switch (httpResponse.getStatus()) {
            case HttpStatusCode.OK:
                response = getResponseObject(httpResponse.getBody());
                break;
            case HttpStatusCode.Conflict:
                ErroDTO erroDTO = XmlHandler.deSerializeXML2ErroDTO(httpResponse.getBody());
                response = new Response(HttpStatusCode.Conflict, erroDTO.getMensagemErro());
                break;
        }
        return response;
    }

    @Override
    protected void onPostExecute(Response result) {
        super.onPostExecute(result);
        if (result != null) {
            Object object = result.getBody();
            switch (result.getStatus()) {
                case HttpStatusCode.OK:
                    onPostExecuteProcessDataUI(object);
                    break;
                case HttpStatusCode.Conflict:
                    if (object instanceof String) {
                        String message = (String) object;
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                    }
                    break;
                default:
                    Toast.makeText(context, Utils.UNKNOWN_ACTION, Toast.LENGTH_LONG).show();
                    break;
            }
        }
        pb.setVisibility(ProgressBar.GONE);
        context = null;
    }

    protected void onPostExecuteProcessDataUI(Object object) {

    }

    protected Response getResponseObject(String httpResponse) {
        throw new UnsupportedOperationException("A operação getResponseObject tem de ser reescrita.");
    }
}
