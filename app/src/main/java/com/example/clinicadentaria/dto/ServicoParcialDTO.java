package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"codigoConsulta", "tipoServico", "data", "hora", "identificacaoCliente"})
@Root(name = "servico")
public class ServicoParcialDTO {

    @Element(name = "tipoServico")
    private String tipoServico;
    @Element(name = "hora")
    private HoraDTO hora;
    @Element(name = "data")
    private DataDTO data;
    @Element(name = "identificacaoCliente")
    private long identificacaoCLiente;
    @Element(name = "codigoConsulta")
    private int codigoConsulta;


    public ServicoParcialDTO(){
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public long getIdentificacaoCLiente() {
        return identificacaoCLiente;
    }

    public void setIdentificacaoCLiente(long identificacaoCLiente) {
        this.identificacaoCLiente = identificacaoCLiente;
    }

    public HoraDTO getHora() {
        return hora;
    }

    public void setHora(HoraDTO hora) {
        this.hora = hora;
    }

    public int getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(int codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }
}
