package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.ArrayList;

@Root(name = "servicos")
public class ListaServicoParcialDTO {

    @ElementList(inline = true, required=false)
    private ArrayList<ServicoParcialDTO> servicos;

    public ListaServicoParcialDTO() {
    }

    public ArrayList<ServicoParcialDTO> getServicos() {
        return servicos;
    }

    public void setServicos(ArrayList<ServicoParcialDTO> servicos) {
        this.servicos = servicos;
    }

}
