package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"nome", "taxa"})
@Root(name = "taxaUrgencia")
public class TaxaUrgenciaDTO {

    @Element(name = "nome")
    private String nomeClinica;
    @Element(name = "taxa")
    private String taxaUrgencia;

    public TaxaUrgenciaDTO(String nomeClinica, String taxaUrgencia){
        this.taxaUrgencia = taxaUrgencia;
        this.nomeClinica = nomeClinica;
    }

    public TaxaUrgenciaDTO(){}

    public String getNomeClinica() {
        return nomeClinica;
    }

    public void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    public String getTaxaUrgencia() {
        return taxaUrgencia;
    }

    public void setTaxaUrgencia(String taxaUrgencia) {
        this.taxaUrgencia = taxaUrgencia;
    }

}
