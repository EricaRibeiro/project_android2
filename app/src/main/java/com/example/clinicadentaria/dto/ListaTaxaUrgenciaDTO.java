package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.ArrayList;

@Root(name = "taxasUrgencias")
public class ListaTaxaUrgenciaDTO {

    @ElementList(inline = true, required=false)
    private ArrayList<TaxaUrgenciaDTO> taxas;

    public ListaTaxaUrgenciaDTO() {
    }

    public ArrayList<TaxaUrgenciaDTO> getTaxas() {
        return taxas;
    }

    public void setTaxas(ArrayList<TaxaUrgenciaDTO> taxas) {
        this.taxas = taxas;
    }

}
