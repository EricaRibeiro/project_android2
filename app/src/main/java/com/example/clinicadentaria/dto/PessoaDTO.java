package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.exception.IdentificacaoInvalidaException;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"nome", "cartaoCidadao", "data"})
@Root(name = "pessoa")
public class PessoaDTO {

    @Element(name = "nome")
    private String nome;
    @Element(name = "cartaoCidadao")
    private long cartaoCidadao;
    @Element(name = "data")
    private DataDTO dataNascimento;

    public PessoaDTO(String nome, long cartaoCidadao, DataDTO dataNascimento){
        this.nome = nome;
        this.cartaoCidadao = cartaoCidadao;
        this.dataNascimento = dataNascimento;
    }

    public PessoaDTO(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCartaoCidadao() {
        return cartaoCidadao;
    }

    public void setCartaoCidadao(long cartaoCidadao) {
        if(cartaoCidadao >= 100000000 && cartaoCidadao <= 999999999)
            this.cartaoCidadao = cartaoCidadao;
        else
            throw new IdentificacaoInvalidaException("O cartão de cidadão deve ser composto por 9 dígitos");
    }

    public DataDTO getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(DataDTO dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
