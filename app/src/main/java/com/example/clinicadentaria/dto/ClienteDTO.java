package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.exception.IdentificacaoInvalidaException;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"identificacaoDentista"})
@Root(name = "cliente")
public class ClienteDTO extends PessoaDTO{

    @Element(name = "identificacaoDentista")
    private long identificacaoDentista;

    public ClienteDTO(){super();}

    public ClienteDTO(String nome, long cartaoCidadao, DataDTO dataNascimento, long identificacaoDentista){
        super(nome, cartaoCidadao, dataNascimento);
        this.identificacaoDentista = identificacaoDentista;
    }

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        if(identificacaoDentista >= 100000000 && identificacaoDentista <= 999999999)
            this.identificacaoDentista = identificacaoDentista;
        else
            throw new IdentificacaoInvalidaException("A identificação do dentista (CC) deve ser composta por 9 dígitos");
    }
}
