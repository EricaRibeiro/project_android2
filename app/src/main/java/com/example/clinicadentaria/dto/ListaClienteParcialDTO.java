package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.ArrayList;

@Root(name = "clientes")
public class ListaClienteParcialDTO {

    @ElementList(inline = true, required=false)
    private ArrayList<ClienteParcialDTO> clientes;

    public ListaClienteParcialDTO() {
    }

    public ArrayList<ClienteParcialDTO> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<ClienteParcialDTO> clientes) {
        this.clientes = clientes;
    }
}
