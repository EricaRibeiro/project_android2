package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.exception.CodigoConsultaInvalidoException;
import com.example.clinicadentaria.exception.HoraInvalidaException;
import com.example.clinicadentaria.exception.TipoServicoInvalidoException;
import com.example.clinicadentaria.model.Data;
import com.example.clinicadentaria.model.Hora;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

import java.util.Locale;

@Order(elements = {"codigoConsulta", "tipoServico", "data", "hora", "identificacaoCliente", "preco"})
@Root(name = "servico")
public class ServicoDTO {

    @Element(name = "tipoServico")
    private String tipoServico;
    @Element(name = "preco")
    private int preco;
    @Element(name = "data")
    private DataDTO data;
    @Element(name = "hora")
    private HoraDTO hora;
    @Element(name = "identificacaoCliente")
    private long identificacaoCLiente;
    @Element(name = "codigoConsulta")
    private int codigoConsulta;

    public ServicoDTO(){

    }

    public ServicoDTO(String tipoServico, int preco, DataDTO dataRealizacao, HoraDTO horaRealizacao, long identificacaoCliente, int codigoConsulta){
        this.tipoServico = tipoServico;
        this.preco = preco;
        this.data = dataRealizacao;
        if(Hora.validarMinutos(horaRealizacao.getMinutos()) && Hora.validarHora(horaRealizacao.getHora(), tipoServico, Converter.dataDTO2Data(dataRealizacao)))
            this.hora = horaRealizacao;
        else{
            throw new HoraInvalidaException("Impossível proceder ao agendamento da consulta! A hora inserida não é válida!");
        }
        this.identificacaoCLiente = identificacaoCliente;
        this.codigoConsulta = codigoConsulta;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        String novaString = tipoServico.toLowerCase(Locale.ROOT).trim().replace(" ", "");
        if(!novaString.equals("limpeza") && !novaString.equals("consultaderotina") && !novaString.equals("colocaçãodeaparelho") && !novaString.equals("manutençãodeaparelho") && !novaString.equals("desvitalização") && !novaString.equals("urgência") && !novaString.equals("removerdente"))
            throw new TipoServicoInvalidoException("A clínica não oferece o serviço introduzido");
        else
            this.tipoServico = tipoServico;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public HoraDTO getHora() {
        return hora;
    }

    public void setHora(HoraDTO hora) {
        this.hora = hora;
    }

    public long getIdentificacaoCLiente() {
        return identificacaoCLiente;
    }

    public void setIdentificacaoCLiente(long identificacaoCLiente) {
        this.identificacaoCLiente = identificacaoCLiente;
    }

    public int getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(int codigoConsulta) {
        if(codigoConsulta >= 1000 && codigoConsulta <= 9999)
            this.codigoConsulta = codigoConsulta;
        else
            throw new CodigoConsultaInvalidoException("O código de consulta deve ser composto por 4 dígitos");
    }
}
