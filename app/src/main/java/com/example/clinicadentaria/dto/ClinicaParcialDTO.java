package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"nome", "nif"})
@Root(name = "clinica")
public class ClinicaParcialDTO {

    @Element(name = "nome")
    private String nome;
    @Element(name = "nif")
    private long nif;

    public ClinicaParcialDTO(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getNif() {
        return nif;
    }

    public void setNif(long nif) {
        this.nif = nif;
    }

}
