package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"nomeClinica", "numeroUrgencias"})
@Root(name = "urgenciaMes")
public class UrgenciasMesDTO {

    @Element(name = "nomeClinica")
    private String nomeClinica;
    @Element(name = "numeroUrgencias")
    private int numeroUrgencias;

    public UrgenciasMesDTO(){}

    public UrgenciasMesDTO(String nomeClinica, int numeroUrgencias){
        this.nomeClinica = nomeClinica;
        this.numeroUrgencias = numeroUrgencias;
    }

    public String getNomeClinica() {
        return nomeClinica;
    }

    public void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    public int getNumeroUrgencias() {
        return numeroUrgencias;
    }

    public void setNumeroUrgencias(int numeroUrgencias) {
        this.numeroUrgencias = numeroUrgencias;
    }

}
