package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"hora", "minutos"})
@Root(name = "horario")
public class HoraDTO {

    @Element(name = "hora")
    private int hora;
    @Element(name = "minutos")
    private int minutos;

    public HoraDTO(){
    }

    public HoraDTO(int hora, int minutos){
        this.hora = hora;
        this.minutos = minutos;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

}
