package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.exception.NifInvalidoException;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"nome", "nif", "data"})
@Root(name = "clinica")
public class ClinicaDTO {

    @Element(name = "nome")
    private String nome;
    @Element(name = "nif")
    private long nif;
    @Element(name = "data")
    private DataDTO dataConstituicao;

    public ClinicaDTO(){
    }

    public ClinicaDTO(String nome, long nif, DataDTO dataConstituicao){
        this.nome = nome;
        this.nif = nif;
        this.dataConstituicao = dataConstituicao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getNif() {
        return nif;
    }

    public void setNif(long nif) {
      if(nif >= 100000000 && nif <= 999999999)
        this.nif = nif;
      else
          throw new NifInvalidoException("O NIF da clínica deve ser composto por 9 dígitos");
    }

    public DataDTO getDataConstituicao() {
        return dataConstituicao;
    }

    public void setDataConstituicao(DataDTO dataConstituicao) {
        this.dataConstituicao = dataConstituicao;
    }

}
