package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.ArrayList;

@Root(name = "urgenciasMes")
public class ListaUrgenciasMesDTO {

    @ElementList(inline = true, required=false)
    private ArrayList<UrgenciasMesDTO> nUrgencias;

    public ListaUrgenciasMesDTO() {
    }

    public ArrayList<UrgenciasMesDTO> getnUrgencias() {
        return nUrgencias;
    }

    public void setnUrgencias(ArrayList<UrgenciasMesDTO> nUrgencias) {
        this.nUrgencias = nUrgencias;
    }

}
