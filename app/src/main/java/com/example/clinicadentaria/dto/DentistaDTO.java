package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.exception.IdentificacaoInvalidaException;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"numeroCedula"})
@Root(name = "dentista")
public class DentistaDTO extends PessoaDTO{

    @Element(name = "numeroCedula")
    private long numeroCedula;

    public DentistaDTO(String nome, long cartaoCidadao, DataDTO dataNascimento, long numeroCedula){
        super(nome, cartaoCidadao, dataNascimento);
        this.numeroCedula = numeroCedula;
    }

    public DentistaDTO(){
        super();
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        if(numeroCedula >= 100000000 && numeroCedula <= 999999999)
            this.numeroCedula = numeroCedula;
        else
            throw new IdentificacaoInvalidaException("O número de cédula deve ser composto por 9 dígitos");
    }
}
