package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"numeroCedula"})
@Root(name = "dentista")
public class DentistaParcialDTO extends PessoaParcialDTO{

    @Element(name = "numeroCedula")
    private long numeroCedula;

    public DentistaParcialDTO(){
        super();
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        this.numeroCedula = numeroCedula;
    }
}
