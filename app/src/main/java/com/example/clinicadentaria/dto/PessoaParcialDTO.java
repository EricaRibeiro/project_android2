package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;


@Order(elements = {"nome", "cartaoCidadao"})
@Root(name = "pessoa")
public class PessoaParcialDTO {

    @Element(name = "nome")
    private String nome;
    @Element(name = "cartaoCidadao")
    private long cartaoCidadao;

    public PessoaParcialDTO(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCartaoCidadao() {
        return cartaoCidadao;
    }

    public void setCartaoCidadao(long cartaoCidadao) {
        this.cartaoCidadao = cartaoCidadao;
    }
}
