package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.model.*;
import com.example.clinicadentaria.model.parcial.*;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.util.ArrayList;

public class Converter {

    public static Data dataDTO2Data(DataDTO data){
        return new Data(data.getDia(), data.getMes(), data.getAno());
    }

    public static DataDTO data2DataDTO(Data data){
        return new DataDTO(data.getDia(), data.getMes(), data.getAno());
    }

    public static Hora horaDTO2Hora(HoraDTO hora){
        return new Hora(hora.getHora(), hora.getMinutos());
    }

    public static HoraDTO hora2HoraDTO(Hora hora){
        return new HoraDTO(hora.getHora(), hora.getMinutos());
    }

    public static ListaClinicaParcial listaClinicaParcialDTO2ListaClinicaParcial(ListaClinicaParcialDTO listaClinicaParcialDTO) throws NullPointerException {
        ArrayList<ClinicaParcial> clinicasParciais = new ArrayList<>();
        ArrayList<ClinicaParcialDTO> clinicasDTO = listaClinicaParcialDTO.getClinicas();
        if(clinicasDTO != null) {
            for (ClinicaParcialDTO clinicaParcialDTO : clinicasDTO) {
                try {
                    ClinicaParcial clinicaParcial = clinicaParcialDTO2ClinicaParcial(clinicaParcialDTO);
                    clinicasParciais.add(clinicaParcial);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaClinicaParcial listaClinicaParcial = new ListaClinicaParcial(clinicasParciais);
        return listaClinicaParcial;
    }

    public static ClinicaParcial clinicaParcialDTO2ClinicaParcial(ClinicaParcialDTO clinicaParcialDTO){
        return new ClinicaParcial(clinicaParcialDTO.getNome(), clinicaParcialDTO.getNif());
    }

    public static Clinica clinicaDTO2Clinica(ClinicaDTO clinicaDTO) throws NullPointerException {
        Data data = dataDTO2Data(clinicaDTO.getDataConstituicao());
        return new Clinica(clinicaDTO.getNome(), data, clinicaDTO.getNif());
    }

    public static ClinicaDTO clinica2ClinicaDTO(Clinica clinica) throws NullPointerException {
        ClinicaDTO clinicaDTO = new ClinicaDTO();
        clinicaDTO.setNif(clinica.getNif());
        clinicaDTO.setNome(clinica.getNome());
        DataDTO dataDTO = data2DataDTO(clinica.getDataConstituicao());
        clinicaDTO.setDataConstituicao(dataDTO);
        return clinicaDTO;
    }

    public static ListaDentistaParcial listaDentistaParcialDTO2ListaDentistaParcial(ListaDentistaParcialDTO listaDentistaPartialDTO) throws NullPointerException {
        ArrayList<DentistaParcial> dentistasParciais = new ArrayList<>();
        ArrayList<DentistaParcialDTO> dentistas = listaDentistaPartialDTO.getDentistas();
        if(dentistas != null) {
            for (DentistaParcialDTO dentista : dentistas) {
                try {
                    DentistaParcial funcionarioPartial = dentistaParcialDTO2DentistaParcial(dentista);
                    dentistasParciais.add(funcionarioPartial);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaDentistaParcial listaDentistaParcial = new ListaDentistaParcial();
        listaDentistaParcial.setDentistaParciais(dentistasParciais);
        return listaDentistaParcial;
    }

    public static DentistaParcial dentistaParcialDTO2DentistaParcial(DentistaParcialDTO funcionarioPartialDTO) throws NullPointerException {
        DentistaParcial dentistaParcial = new DentistaParcial();
        dentistaParcial.setNome(funcionarioPartialDTO.getNome());
        dentistaParcial.setNumeroCedula(funcionarioPartialDTO.getNumeroCedula());
        dentistaParcial.setCartaoCidadao(funcionarioPartialDTO.getCartaoCidadao());

        return dentistaParcial;
    }

    public static Dentista dentistaDTO2Dentista(DentistaDTO dentistaDTO) throws NullPointerException {
        Dentista dentista = new Dentista();

        Data data = dataDTO2Data(dentistaDTO.getDataNascimento());
        dentista.setNumeroCedula(dentistaDTO.getNumeroCedula());
        dentista.setNome(dentistaDTO.getNome());
        dentista.setCartaoCidadao(dentistaDTO.getCartaoCidadao());
        dentista.setDataNascimento(data);
        return dentista;
    }

    public static DentistaDTO dentista2DentistaDTO(Dentista dentista) throws NullPointerException {
        DentistaDTO dentistaDTO = new DentistaDTO();
        dentistaDTO.setCartaoCidadao(dentista.getCartaoCidadao());
        dentistaDTO.setNome(dentista.getNome());
        DataDTO dataDTO = data2DataDTO(dentista.getDataNascimento());
        dentistaDTO.setDataNascimento(dataDTO);
        dentistaDTO.setNumeroCedula(dentista.getNumeroCedula());
        return dentistaDTO;
    }

    public static Cliente clienteDTO2Cliente(ClienteDTO clienteDTO) throws NullPointerException {
        Cliente cliente = new Cliente();

        Data data = dataDTO2Data(clienteDTO.getDataNascimento());
        cliente.setCartaoCidadao(clienteDTO.getCartaoCidadao());
        cliente.setNome(clienteDTO.getNome());
        cliente.setDataNascimento(data);
        cliente.setIdentificacaoDentista(clienteDTO.getIdentificacaoDentista());
        return cliente;
    }

    public static ClienteDTO cliente2ClienteDTO(Cliente cliente) throws NullPointerException {
        ClienteDTO clienteDTO = null;

        DataDTO dataDTO = data2DataDTO(cliente.getDataNascimento());
        clienteDTO = new ClienteDTO(cliente.getNome(), cliente.getCartaoCidadao(), dataDTO, cliente.getIdentificacaoDentista());

        return clienteDTO;
    }

    public static ListaClienteParcial listaClienteParcialDTO2ListaClienteParcial(ListaClienteParcialDTO listaCLienteParcialDTO) throws NullPointerException {
        ArrayList<ClienteParcial> clientesParciais = new ArrayList<>();
        ArrayList<ClienteParcialDTO> clientes = listaCLienteParcialDTO.getClientes();
        if(clientes != null) {
            for (ClienteParcialDTO cliente : clientes) {
                try {
                    ClienteParcial pessoaParcial = clienteParcialDTO2ClienteParcial(cliente);
                    clientesParciais.add(pessoaParcial);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaClienteParcial listaClienteParcial = new ListaClienteParcial();
        listaClienteParcial.setClientesParciais(clientesParciais);
        return listaClienteParcial;
    }

    public static ClienteParcial clienteParcialDTO2ClienteParcial(ClienteParcialDTO pessoaParcialDTO) throws NullPointerException {
        ClienteParcial clienteParcial = new ClienteParcial();
        clienteParcial.setNome(pessoaParcialDTO.getNome());
        clienteParcial.setIdentificacaoDentista(pessoaParcialDTO.getIdentificacaoDentista());
        clienteParcial.setCartaoCidadao(pessoaParcialDTO.getCartaoCidadao());

        return clienteParcial;
    }

    public static ListaServicoParcial listaServicoParcialDTO2ListaServicoParcial(ListaServicoParcialDTO listaServicoPartialDTO) throws NullPointerException {
        ArrayList<ServicoParcial> servicosParciais = new ArrayList<>();
        ArrayList<ServicoParcialDTO> servicos = listaServicoPartialDTO.getServicos();
        if(servicos != null) {
            for (ServicoParcialDTO servico : servicos) {
                try {
                    ServicoParcial servicoParcial = servicoParcialDTO2ServicoParcial(servico);
                    servicosParciais.add(servicoParcial);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaServicoParcial listaServicoParcial = new ListaServicoParcial();
        listaServicoParcial.setServicoParciais(servicosParciais);
        return listaServicoParcial;
    }

    public static ServicoParcial servicoParcialDTO2ServicoParcial(ServicoParcialDTO servicoParcialDTO) throws NullPointerException {
        ServicoParcial servicoParcial = new ServicoParcial();
        servicoParcial.setTipoServico(servicoParcialDTO.getTipoServico());
        servicoParcial.setIdentificacaoCliente(servicoParcialDTO.getIdentificacaoCLiente());
        servicoParcial.setCodigoConsulta(servicoParcialDTO.getCodigoConsulta());
        Data d = dataDTO2Data(servicoParcialDTO.getData());
        servicoParcial.setDataRealizacao(d);
        Hora h = horaDTO2Hora(servicoParcialDTO.getHora());
        servicoParcial.setHoraRealizacao(h);

        return servicoParcial;
    }

    public static Servico servicoDTO2Servico(ServicoDTO servicoDTO) throws NullPointerException {
        Servico servico = new Servico();

        Data data = dataDTO2Data(servicoDTO.getData());
        Hora hora = horaDTO2Hora(servicoDTO.getHora());
        servico.setCodigoConsulta(servicoDTO.getCodigoConsulta());
        servico.setTipoServico(servicoDTO.getTipoServico());
        servico.setIdentificacaoCliente(servicoDTO.getIdentificacaoCLiente());
        servico.setDataRealizacao(data);
        servico.setPreco(servicoDTO.getPreco());
        servico.setHoraRealizacao(hora);
        return servico;
    }

    public static ServicoDTO servico2ServicoDTO(Servico servico) throws NullPointerException {
        ServicoDTO servicoDTO = null;

        DataDTO dataDTO = data2DataDTO(servico.getDataRealizacao());
        HoraDTO horaDTO = hora2HoraDTO(servico.getHoraRealizacao());

        servicoDTO = new ServicoDTO(servico.getTipoServico(), servico.getPreco(), dataDTO, horaDTO, servico.getIdentificacaoCliente(), servico.getCodigoConsulta());

        /*servicoDTO.setTipoServico(servico.getTipoServico());
        servicoDTO.setCodigoConsulta(servico.getCodigoConsulta());
        servicoDTO.setData(dataDTO);
        servicoDTO.setHora(horaDTO);
        servicoDTO.setPreco(servico.getPreco());
        servicoDTO.setIdentificacaoCLiente(servico.getIdentificacaoCliente());*/
        return servicoDTO;
    }

    public static AvaliacaoDTO avaliacao2AvaliacaoDTO(Avaliacao avaliacao) throws NullPointerException {
        AvaliacaoDTO avaliacaoDTO = new AvaliacaoDTO();
        avaliacaoDTO.setAvaliacao(avaliacao.getAvaliacao());
        avaliacaoDTO.setIdentificacaoCliente(avaliacao.getIdentificacaoCliente());
        return avaliacaoDTO;
    }

    public static ListaTaxaUrgencia listaTaxaUrgenciaDTO2ListaTaxaUrgencia(ListaTaxaUrgenciaDTO listaTaxaUrgenciaDTO) throws NullPointerException {
        ArrayList<TaxaUrgencia> taxasUrgencia = new ArrayList<>();
        ArrayList<TaxaUrgenciaDTO> taxasDTO = listaTaxaUrgenciaDTO.getTaxas();
        if(taxasDTO != null) {
            for (TaxaUrgenciaDTO taxa : taxasDTO) {
                try {
                    TaxaUrgencia taxaU = taxaUrgenciaDTO2TaxaUrgencia(taxa);
                    taxasUrgencia.add(taxaU);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaTaxaUrgencia listaTaxaUrgencia = new ListaTaxaUrgencia();
        listaTaxaUrgencia.setTaxaUrgencias(taxasUrgencia);
        return listaTaxaUrgencia;
    }

    public static TaxaUrgencia taxaUrgenciaDTO2TaxaUrgencia(TaxaUrgenciaDTO taxaUrgenciaDTO) throws NullPointerException {
        TaxaUrgencia taxaUrgencia = new TaxaUrgencia();
        taxaUrgencia.setTaxaUrgencia(taxaUrgenciaDTO.getTaxaUrgencia());
        taxaUrgencia.setNomeClinica(taxaUrgenciaDTO.getNomeClinica());

        return taxaUrgencia;
    }

    public static ListaUrgenciasMes listaUrgenciasMesDTO2ListaUrgenciasMes(ListaUrgenciasMesDTO listaUrgenciasMesDTO) throws NullPointerException {
        ArrayList<UrgenciasMes> urgenciasMes = new ArrayList<>();
        ArrayList<UrgenciasMesDTO> urgenciasDTO = listaUrgenciasMesDTO.getnUrgencias();
        if(urgenciasDTO != null) {
            for (UrgenciasMesDTO urgencia : urgenciasDTO) {
                try {
                    UrgenciasMes uMes = urgenciasMesDTO2UrgenciasMes(urgencia);
                    urgenciasMes.add(uMes);
                } catch (NullPointerException e) {
                    //does nothing. Actually, nothing is added to arraylist
                }
            }
        }
        ListaUrgenciasMes listaUrgenciasMes = new ListaUrgenciasMes();
        listaUrgenciasMes.setUrgenciasMes(urgenciasMes);
        return listaUrgenciasMes;
    }

    public static UrgenciasMes urgenciasMesDTO2UrgenciasMes(UrgenciasMesDTO urgenciasMesDTO) throws NullPointerException {
        UrgenciasMes urgenciasMes = new UrgenciasMes();
        urgenciasMes.setNumeroUrgencias(urgenciasMesDTO.getNumeroUrgencias());
        urgenciasMes.setNomeClinica(urgenciasMesDTO.getNomeClinica());

        return urgenciasMes;
    }
}
