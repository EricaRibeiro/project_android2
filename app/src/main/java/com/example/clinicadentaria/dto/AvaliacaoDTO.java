package com.example.clinicadentaria.dto;
import com.example.clinicadentaria.exception.AvaliacaoInvalidaException;
import com.example.clinicadentaria.exception.IdentificacaoInvalidaException;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"identificacaoCliente", "avaliacao"})
@Root(name = "avaliacaoClinica")
public class AvaliacaoDTO {

    @Element(name = "identificacaoCliente")
    private long identificacaoCliente;
    @Element(name = "avaliacao")
    private int avaliacao;

    public AvaliacaoDTO(){

    }

    public AvaliacaoDTO(int avaliacao, long identificacaoCliente){
        this.avaliacao = avaliacao;
        this.identificacaoCliente = identificacaoCliente;
    }

    public long getIdentificacaoCliente() {
        return identificacaoCliente;
    }

    public void setIdentificacaoCliente(long identificacaoCliente) {
        if(identificacaoCliente >= 100000000 && identificacaoCliente <= 999999999)
            this.identificacaoCliente = identificacaoCliente;
        else
            throw new IdentificacaoInvalidaException("A identificação do cliente (CC) deve ser composta por 9 dígitos");
    }

    public int getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(int avaliacao) {
        if(avaliacao > 0 && avaliacao < 6)
            this.avaliacao = avaliacao;
        else
            throw new AvaliacaoInvalidaException("A avaliação do cliente deve estar compreendida entre 1 e 5");
    }

}
