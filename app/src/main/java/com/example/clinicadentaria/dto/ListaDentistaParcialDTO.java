package com.example.clinicadentaria.dto;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import java.util.ArrayList;

@Root(name = "dentistas")
public class ListaDentistaParcialDTO {

    @ElementList(inline = true, required=false)
    private ArrayList<DentistaParcialDTO> dentistas;

    public ListaDentistaParcialDTO() {
    }

    public ArrayList<DentistaParcialDTO> getDentistas() {
        return dentistas;
    }

    public void setDentistas(ArrayList<DentistaParcialDTO> dentistas) {
        this.dentistas = dentistas;
    }
}
