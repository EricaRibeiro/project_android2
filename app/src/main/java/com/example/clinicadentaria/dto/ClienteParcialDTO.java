package com.example.clinicadentaria.dto;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Order;
import org.simpleframework.xml.Root;

@Order(elements = {"identificacaoDentista"})
@Root(name = "cliente")
public class ClienteParcialDTO extends PessoaParcialDTO{

    @Element(name = "identificacaoDentista")
    private long identificacaoDentista;

    public ClienteParcialDTO(){super();}

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        this.identificacaoDentista = identificacaoDentista;
    }
}
